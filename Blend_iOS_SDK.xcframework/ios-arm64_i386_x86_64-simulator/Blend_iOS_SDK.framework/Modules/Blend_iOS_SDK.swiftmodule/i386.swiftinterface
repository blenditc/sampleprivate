// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4.2 (swiftlang-1205.0.28.2 clang-1205.0.19.57)
// swift-module-flags: -target i386-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Blend_iOS_SDK
@_exported import Blend_iOS_SDK
import DTBiOSSDK
import FBAudienceNetwork
import FacebookAdapter
import FirebaseRemoteConfig
import Foundation
import GoogleMobileAds
import IASDKCore
import MoPubAdapter
import MoPubSDK
import Swift
import UIKit
import os.log
import os
@objc public protocol BlendAdViewDelegate {
  @objc func adDidRecieve()
  @objc func adDidFail(with error: Blend_iOS_SDK.BlendError)
}
@_hasMissingDesignatedInitializers @objc public class BlendAdView : UIKit.UIView {
  @objc weak public var delegate: Blend_iOS_SDK.BlendAdViewDelegate?
  @objc public var customAdView: GoogleMobileAds.GADNativeAdView?
  @objc public var nativeAdCustomization: Blend_iOS_SDK.BlendNativeAdCustomization
  @objc public init(rootViewController: UIKit.UIViewController, placementName: Swift.String, adType: Blend_iOS_SDK.BlendAdType)
  @objc public func getAd()
  @objc public func start()
  @objc public func stop()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc deinit
}
extension BlendAdView : GoogleMobileAds.GADBannerViewDelegate {
  @objc dynamic public func bannerViewDidReceiveAd(_ bannerView: GoogleMobileAds.GADBannerView)
  @objc dynamic public func bannerView(_ bannerView: GoogleMobileAds.GADBannerView, didFailToReceiveAdWithError error: Swift.Error)
  @objc dynamic public func bannerViewDidRecordImpression(_ bannerView: GoogleMobileAds.GADBannerView)
  @objc dynamic public func bannerViewWillPresentScreen(_ bannerView: GoogleMobileAds.GADBannerView)
  @objc dynamic public func bannerViewWillDismissScreen(_ bannerView: GoogleMobileAds.GADBannerView)
  @objc dynamic public func bannerViewDidDismissScreen(_ bannerView: GoogleMobileAds.GADBannerView)
}
extension BlendAdView : GoogleMobileAds.GADNativeAdDelegate {
  @objc dynamic public func nativeAdDidRecordImpression(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdDidRecordClick(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdWillPresentScreen(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdWillDismissScreen(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdDidDismissScreen(_ nativeAd: GoogleMobileAds.GADNativeAd)
  public func nativeAdWillLeaveApplication(_ nativeAd: GoogleMobileAds.GADNativeAd)
}
@objc public enum BlendAdType : Swift.Int {
  case small
  case mrec
  case interstitial
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public class BlendAdConfigKeys : ObjectiveC.NSObject {
  final public let localFileName: Swift.String
  final public let adConfigKey: Swift.String
  @objc public init(localFileName: Swift.String, adConfigKey: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
@objc public protocol BlendFullScreenContentDelegate {
  @objc func adDidRecordClick()
  @objc func adDidRecordImpression()
  @objc func adDidPresentFullScreenContent()
  @objc func adDidDismissFullScreenContent()
  @objc func adWillDismissFullScreenContent()
  @objc func asDidFailToPresentFullScreenContentWithError(error: Swift.Error)
}
extension BlendInterstitialAd : GoogleMobileAds.GADFullScreenContentDelegate {
  @objc dynamic public func adDidRecordClick(_ ad: GoogleMobileAds.GADFullScreenPresentingAd)
  @objc dynamic public func adDidRecordImpression(_ ad: GoogleMobileAds.GADFullScreenPresentingAd)
  @objc dynamic public func adDidPresentFullScreenContent(_ ad: GoogleMobileAds.GADFullScreenPresentingAd)
  @objc dynamic public func adDidDismissFullScreenContent(_ ad: GoogleMobileAds.GADFullScreenPresentingAd)
  @objc dynamic public func adWillDismissFullScreenContent(_ ad: GoogleMobileAds.GADFullScreenPresentingAd)
  @objc dynamic public func ad(_ ad: GoogleMobileAds.GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Swift.Error)
}
public typealias BlendInterstitialCompletionHandler = (Blend_iOS_SDK.BlendInterstitialAd?, Swift.Error?) -> ()
@_hasMissingDesignatedInitializers @objc public class BlendInterstitialAd : ObjectiveC.NSObject {
  @objc public var appEventDelegate: Blend_iOS_SDK.BlendViewAppEventDelegate? {
    @objc get
    @objc set
  }
  @objc public var adUnitID: Swift.String? {
    @objc get
  }
  @objc public var responseInfo: Blend_iOS_SDK.BlendResponseInfo? {
    @objc get
  }
  @objc public var fullScreenContentDelegate: Blend_iOS_SDK.BlendFullScreenContentDelegate? {
    @objc get
    @objc set
  }
  @objc public class func loadWith(placementName: Swift.String, blendRequest: Blend_iOS_SDK.BlendRequest, completion: @escaping Blend_iOS_SDK.BlendInterstitialCompletionHandler)
  public func present(fromRootViewController: UIKit.UIViewController)
  @objc override dynamic public init()
  @objc deinit
}
extension BlendInterstitialAd : GoogleMobileAds.GADAppEventDelegate {
  @objc dynamic public func adView(_ banner: GoogleMobileAds.GADBannerView, didReceiveAppEvent name: Swift.String, withInfo info: Swift.String?)
  @objc dynamic public func interstitialAd(_ interstitialAd: GoogleMobileAds.GADInterstitialAd, didReceiveAppEvent name: Swift.String, withInfo info: Swift.String?)
}
public class Scheduler {
  public typealias SchedulerAction = () -> ()
  public var interval: Foundation.TimeInterval {
    get
    set
  }
  public var name: Swift.String {
    get
    set
  }
  public var onlyCountTimeWhenRunning: Swift.Bool {
    get
  }
  public var running: Swift.Bool {
    get
  }
  public init(with name: Swift.String, interval: Foundation.TimeInterval, onlyCountTimeWhenRunning: Swift.Bool, action: @escaping Blend_iOS_SDK.Scheduler.SchedulerAction)
  public func start()
  public func stop()
  public func resetTimer()
  public func fireEvent()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class BlendRequest : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum BlendError : Swift.Int, Swift.Error {
  case adsDisabled
  case rootVC
  case adPlacementDisabled
  case adUnitNotFound
  case adFormatNotRecognized
  case adUnitError
  case unknown
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public static var _nsErrorDomain: Swift.String {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
extension BlendError : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class BlendNativeAdCustomization : ObjectiveC.NSObject {
  @objc public var backgroundColor: UIKit.UIColor
  @objc public var headlineTextColor: UIKit.UIColor
  @objc public var advertiserTextColor: UIKit.UIColor
  @objc public var bodyTextColor: UIKit.UIColor
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum BlendLogLevel : Swift.Int {
  case none
  case debug
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc final public class BlendAdManager : ObjectiveC.NSObject {
  @objc public static let shared: Blend_iOS_SDK.BlendAdManager
  @objc final public var logLevel: Blend_iOS_SDK.BlendLogLevel
  @objc final public func start(amazonAppKey: Swift.String, adConfigKeys: Blend_iOS_SDK.BlendAdConfigKeys)
  @objc deinit
}
@objc public protocol BlendViewAppEventDelegate {
  @objc func adView(didReceiveAppEvent name: Swift.String, withInfo info: Swift.String?)
  @objc func interstitialAd(didReceiveAppEvent name: Swift.String, withInfo info: Swift.String?)
}
public typealias A9SlotId = Swift.String
public protocol BlendNativeAdProtocol : AnyObject {
  func nativeAdDidRecordImpression()
  func nativeAdDidRecordClick()
  func nativeAdWillPresentScreen()
  func nativeAdWillDismissScreen()
  func nativeAdDidDismissScreen()
  func nativeAdIsMuted()
}
@objc @_hasMissingDesignatedInitializers public class BlendNativeAd : ObjectiveC.NSObject {
  public var body: Swift.String? {
    get
  }
  public var headline: Swift.String? {
    get
  }
  public var iconImage: UIKit.UIImage? {
    get
  }
  public var callToAction: Swift.String? {
    get
  }
  public var images: [UIKit.UIImage?]? {
    get
  }
  public var starRating: Swift.Double? {
    get
  }
  public var store: Swift.String? {
    get
  }
  public var price: Swift.String? {
    get
  }
  public var advertiser: Swift.String? {
    get
  }
  public var mediaContent: GoogleMobileAds.GADMediaContent? {
    get
  }
  weak public var delegate: Blend_iOS_SDK.BlendNativeAdProtocol? {
    get
    set
  }
  @objc override dynamic public init()
  @objc deinit
}
extension BlendNativeAd : GoogleMobileAds.GADNativeAdDelegate {
  @objc dynamic public func nativeAdDidRecordImpression(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdDidRecordClick(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdWillPresentScreen(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdWillDismissScreen(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdDidDismissScreen(_ nativeAd: GoogleMobileAds.GADNativeAd)
  @objc dynamic public func nativeAdIsMuted(_ nativeAd: GoogleMobileAds.GADNativeAd)
}
@_hasMissingDesignatedInitializers public class BlendMediaContent {
  @objc deinit
}
@objc public class BlendResponseInfo : ObjectiveC.NSObject {
  public init(_ responseInfo: GoogleMobileAds.GADResponseInfo)
  @objc override dynamic public init()
  @objc deinit
}
extension Blend_iOS_SDK.BlendAdType : Swift.Equatable {}
extension Blend_iOS_SDK.BlendAdType : Swift.Hashable {}
extension Blend_iOS_SDK.BlendAdType : Swift.RawRepresentable {}
extension Blend_iOS_SDK.BlendError : Swift.Equatable {}
extension Blend_iOS_SDK.BlendError : Swift.Hashable {}
extension Blend_iOS_SDK.BlendError : Swift.RawRepresentable {}
extension Blend_iOS_SDK.BlendLogLevel : Swift.Equatable {}
extension Blend_iOS_SDK.BlendLogLevel : Swift.Hashable {}
extension Blend_iOS_SDK.BlendLogLevel : Swift.RawRepresentable {}
