Pod::Spec.new do |spec|

  spec.name         = 'someSDK'
  spec.version      = '1.0.0'
  spec.summary      = 'BlendAd SDK allows for easily displaying ads with native and banner inventory from DFP'

  spec.description  = 'BlendAd SDK allows for easily displaying ads with native and banner inventory from DFP. The SDK offers the ability to show a set of relevant ads using a dfp mediation stack to display higher CPM banners or native ads on a specific placement.'

  spec.homepage     = 'https://www.inmobi.com'
  spec.license      = { :type => 'Apache', :file => 'LICENSE.txt' }

  spec.author       = { "InMobi" => "blend-itc@inmobi.com" }

  spec.platform     = :ios, '10.0'
  spec.swift_versions = '5.0'

  spec.source       = { :git => 'https://gitlab.com/blenditc/sampleprivate.git', :tag => '1.0.0'}
  
  spec.vendored_frameworks = 'Blend_iOS_SDK.xcframework'

  spec.pod_target_xcconfig = { 'VALID_ARCHS' => 'armv7 arm64 x86_64' }
      
  spec.dependency 'Firebase/RemoteConfig'

  spec.dependency 'AmazonPublisherServicesSDK', '~> 4.0'
  spec.dependency 'AmazonPublisherServicesAdMobAdapter', '~> 2.0'

  spec.dependency 'GoogleMobileAdsMediationFacebook'

  spec.dependency 'GoogleMobileAdsMediationFyber'

  spec.dependency 'GoogleMobileAdsMediationMoPub'

  spec.dependency 'GoogleMobileAdsMediationInMobi'


end
